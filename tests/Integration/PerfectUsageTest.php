<?php

use PHPUnit\Framework\TestCase;
use GO\Tar\TarStreamer;

class PerfectUsageTest extends TestCase
{
    public function test_creating_archive()
    {
        $out = gzopen("/dev/null", "wb9");

        $tar = new TarStreamer($out);
        $tar->addExistingFile("/data", __DIR__ . "/data");
        $tar->addExistingFile("/data/folder1/1024.txt", __DIR__ . "/data/folder1/1024.txt");
        $tar->addEndOfArchive();

        $ok = fclose($out);

        // no exceptions or errors
        $this->assertTrue($ok);
    }
}
