<?php

use PHPUnit\Framework\TestCase;
use GO\Tar\TarStreamer;

class TarStreamerTest extends TestCase
{
    protected $in_file;
    protected $tar_file;
    protected $stream_with_tar;

    public function setUp(): void
    {
        parent::setUp();
        $this->in_file = tempnam(sys_get_temp_dir(), __CLASS__);
        file_put_contents($this->in_file, "Test content");

        $this->tar_file = tempnam(sys_get_temp_dir(), __CLASS__);
        // ".tar" is required to help PharData
        rename($this->tar_file, $this->tar_file . ".tar");
        $this->tar_file = $this->tar_file . ".tar";
        // 
        //
        file_put_contents(
            $this->tar_file,
            gzdecode(base64_decode(
                "H4sICKq06l0AAzEudGFyAO3POwrDQAyEYR1lTxCk7MMXyQWM2TJeWCvg49ukCaRIKhc2/9dMoSlG".
                "dfU+Tn57VjmM7kpK79x9p6olsZiHe45WShQ1y3mQoMdN+ngtPvYQpLfmv3r/7if1qIuHqc1e5yu+".
                "BwAAAAAAAAAAAAAAAACXtQHN3eeOACgAAA=="
            ))
        );

        $this->stream_with_tar = tmpfile();
        fwrite( $this->stream_with_tar ,gzdecode(base64_decode(
        "H4sIAAAAAAAAA+3VQQqDMBAF0DlKTlBmTEzOI9hFaSFFUyg9fWNCRVzYLhrTwn+bLCI48fNj34VO".
        "DuEeqByOrDFpjdZr1JJoy84aNtISi2hhUlxwptltDN2gFA3eb36Dd/t/qp/yrz0EVDPl3/xS/7XJ".
        "/W/Q/z2k/GsPAdVcvL+W7lnqv1333i76z3P/W5f67+LjyhSeK/m0//1pPG/t53O4+ZB/4nEcfOl3".
        "5Pztxv2/yJ8l5d9oUrr0YJNv3f/5HEKvHxoAAAAAAAAAAAAAAAAAwM6eQMrd5gAoAAA="
        )));
        fseek($this->stream_with_tar, 0);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        unlink($this->in_file);
        unlink($this->tar_file);
    }

    public function testAddExistingFile()
    {
        $out = fopen($this->tar_file, "w");
        $tar = new TarStreamer($out);

        $tar->addExistingFile("file1.txt", $this->in_file);
        $tar->addExistingFile("file2.txt", $this->in_file);
        $tar->addEndOfArchive();
        fclose($out);

        // проверяем что создан корректный tar
        $phar = new \PharData($this->tar_file, Phar::KEY_AS_FILENAME);
        $this->assertTrue(isset($phar["file1.txt"]));
        $this->assertTrue(isset($phar["file2.txt"]));
        $this->assertEquals("Test content", file_get_contents($phar["file1.txt"]));
        $this->assertEquals("Test content", file_get_contents($phar["file2.txt"]));
    }

    public function testAddExistingDirectory()
    {
        $out = fopen($this->tar_file, "w");
        $tar = new TarStreamer($out);

        $tar->addExistingFile("dir1", __DIR__);
        $tar->addExistingFile("dir2", __DIR__);
        $tar->addEndOfArchive();
        fclose($out);
        
        $phar = new \PharData($this->tar_file, Phar::KEY_AS_FILENAME);

        $this->assertTrue(isset($phar["dir1"]));
        $this->assertTrue(isset($phar["dir2"]));
    }
    
    /**
     * @group this
     */
    public function testAddExistingSymlink()
    {
        $out = fopen($this->tar_file, "w");
        $tar = new TarStreamer($out);

        $link1 = tempnam(sys_get_temp_dir(), __METHOD__);
        unlink($link1);
        symlink(".", $link1);
        $link2 = tempnam(sys_get_temp_dir(), __METHOD__);
        unlink($link2);
        symlink($this->in_file, $link2);

        $tar->addExistingFile("link1", $link1);
        $tar->addExistingFile("link2", $link2);
        $tar->addEndOfArchive();
        fclose($out);

        unlink($link1);
        unlink($link2);
        
        $phar = new \PharData($this->tar_file, Phar::KEY_AS_FILENAME);

        $this->assertTrue(isset($phar["link1"]));
        $this->assertTrue(isset($phar["link2"]));
    }
    
    public function testAddExistingFIFO()
    {
        $out = fopen($this->tar_file, "w");
        $tar = new TarStreamer($out);

        $stat = stat(__DIR__);
        $stat[7] = $stat["mode"] = 0010600;
        $link = tempnam(sys_get_temp_dir(), __METHOD__);
        unlink($link);
        posix_mkfifo ( $link , 0777 );

        $tar->addNonFile($stat, "pipe");
        $tar->addEndOfArchive();
        fclose($out);        
        unlink($link);

        $phar = new \PharData($this->tar_file, Phar::KEY_AS_FILENAME);

        $this->assertTrue(isset($phar["pipe"]));
    }

    public function testLongName()
    {
        $name1 = str_repeat("Long", 20)."/".str_repeat("Long", 10).".File.End";
        $name2 = str_repeat("lONG/", 40)."Dir.End";
        $out = fopen($this->tar_file, "w");
        $tar = new TarStreamer($out);

        $tar->addExistingFile($name1, $this->in_file);
        $tar->addNonFile(stat(__DIR__), $name2);
        $tar->addEndOfArchive();
        fclose($out);
        
        $phar = new \PharData($this->tar_file, Phar::KEY_AS_FILENAME);

        $this->assertTrue(isset($phar[str_repeat("Long", 20)]), "Пропал каталог с длинным именем");
        $this->assertTrue(isset($phar["lONG"]), "Пропал каталог со вложенностью");
    }

    public function testLongNameRead()
    {
        // arrange
        $name1 = str_repeat("Long", 20)."/".str_repeat("Long", 10).".File.End";
        $out = fopen($this->tar_file, "r+w");
        $tar1 = new TarStreamer($out);

        $tar1->addExistingFile($name1, $this->in_file);
        $tar1->addEndOfArchive();
        fseek($out, 0, SEEK_SET);

        // act
        $tar2 = new TarStreamer($out);
        $head = $tar2->readHeader();

        // assert
        $this->assertEquals($name1, $head['pathname']);
    }

    public function testLongNameFail()
    {
        $name1 = str_repeat("Long", 220).".File.End";
        $out = fopen($this->tar_file, "w");
        $tar = new TarStreamer($out);

        $this->expectException(\Exception::class);

        $tar->addExistingFile($name1, $this->in_file);
        $tar->addEndOfArchive();
        fclose($out);        
    }
    public function testLongNamePrefixFail()
    {
        $name1 = str_repeat("Long", 50)."/Ok.File.End";
        $out = fopen($this->tar_file, "w");
        $tar = new TarStreamer($out);

        $this->expectException(\Exception::class);

        $tar->addExistingFile($name1, $this->in_file);
        $tar->addEndOfArchive();
        fclose($out);        
    }

    public function testReadHeader()
    {
        $in = fopen($this->tar_file, "r");
        $tar = new TarStreamer($in);

        $stat = $tar->readHeader();

        $this->assertEquals(strlen("Test content"), $stat["size"]);
        $this->assertEquals(0644, $stat["mode"]);
        $this->assertEquals(0, $stat["ino"]);
        $this->assertEquals(0, $stat["_type"]);
        $this->assertEquals("extract.me", $stat["pathname"]);
    }

    public function testReadData()
    {
        $in = fopen($this->tar_file, "r");
        $tar = new TarStreamer($in);

        $tar->readHeader();
        $data = $tar->readData();
        $tar->readHeader();

        $this->assertEquals("Test content", $data);
        $this->assertTrue($tar->eof());
    }
    
    public function testReadDataToStream()
    {
        $in = fopen($this->tar_file, "r");
        $out = tmpfile();
        $tar = new TarStreamer($in);

        $tar->readHeader();
        $data = $tar->readDataToStream($out);
        $tar->readHeader();
        fseek($out, 0);

        $this->assertEquals("Test content", file_get_contents($this->in_file));
        $this->assertTrue($tar->eof());
    }

    /**
     * @group xpartial
     */    
    public function testGetDataStream()
    {
        $in = fopen($this->tar_file, "r");
        $tar = new TarStreamer($in);

        $stream = $tar->getDataStream();
        $data = fgets($stream);

        $this->assertEquals("Test content", $data);
    }

    /**
     * @group partial
     */
    public function testGetDataStreamPartialRead()
    {
        $tar = new TarStreamer($this->stream_with_tar);

        $stream = $tar->getDataStream();
        $data = fread($stream, 3);
        $head = $tar->readHeader(); // must not fail

        $this->assertEquals("dat", $data);
        $this->assertEquals("data2.txt", $head["pathname"]);
    }

    /**
     * @group xpartial
     */
    public function testGetDataStreamPartialReadTwice()
    {
        $tar = new TarStreamer($this->stream_with_tar);

        $stream = $tar->getDataStream();
        $data1 = fread($stream, 3);
        $data2 = $tar->readData();

        $this->assertEquals("dat", $data1);
        $this->assertEquals("data2", $data2);
    }

    public function testFullRead() {
        $tar = new TarStreamer($this->stream_with_tar);
        $res = [];

        while(true) {
            $head = $tar->readHeader();
            if($tar->eof()) {
                break;
            }
            $data = $tar->readData();
            $res[$head["pathname"]] = $data;
        }

        $this->assertEquals("data1", $res["data1.txt"]);
        $this->assertEquals("data2", $res["data2.txt"]);
        $this->assertEquals(null, $res["loop0"]);
        $this->assertEquals(null, $res["zero"]);
    }
    
    public function testStreamRead() {
        $tar = new TarStreamer($this->stream_with_tar);
        $res = [];

        while(true) {
            $head = $tar->readHeader();
            if($tar->eof()) {
                break;
            }
            $out = tempnam(sys_get_temp_dir(), __METHOD__);
            $tar->readDataToStream(fopen($out,"w"));
            $res[$head["pathname"]] = file_get_contents($out);
            unlink($out);
        }

        $this->assertEquals("data1", $res["data1.txt"]);
        $this->assertEquals("data2", $res["data2.txt"]);
        $this->assertEquals(null, $res["loop0"]);
        $this->assertEquals(null, $res["zero"]);
    }

    public function testFullSkip() {
        $tar = new TarStreamer($this->stream_with_tar);
        $res = [];

        while(true) {
            $head = $tar->readHeader();
            if($tar->eof()) {
                break;
            }
            $res[$head["pathname"]] = $head;
        }

        $this->assertTrue(isset($res["data1.txt"]));
        $this->assertTrue(isset($res["data2.txt"]));
        $this->assertTrue(isset($res["loop0"]));
        $this->assertTrue(isset($res["zero"]));
    }

    public function testFullData() {
        $tar = new TarStreamer($this->stream_with_tar);
        $res = [];

        while(!$tar->eof()) {
            $data = $tar->readData();
            @$res[$data]++;
        }

        $this->assertEquals(1, $res["data1"]);
        $this->assertEquals(1, $res["data2"]);
        $this->assertEquals(3, $res[null]); // zero, loop0, eof

    }

    public function testBadTarFailed() {
        file_put_contents($this->tar_file, str_repeat("1234", 1024) );
        $tar = new TarStreamer(tmpfile());

        $this->expectException(\Exception::class);

        $tar->readHeader();
    }

    public function testShortTarFailed() {
        file_put_contents($this->tar_file, str_repeat("\0", 140) );
        $tar = new TarStreamer(tmpfile());

        $this->expectException(\Exception::class);

        $tar->readHeader();
    }

    public function testAddNotExistsingFailed() {
        $tar = new TarStreamer(tmpfile());

        $this->expectException(\Exception::class);

        $file = tempnam(sys_get_temp_dir(), __METHOD__);
        unlink($file);
        $tar->addExistingFile("file.ext",$file);
    }

}

