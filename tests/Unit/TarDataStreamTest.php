<?php

use PHPUnit\Framework\TestCase;
use GO\Tar\TarDataStream;

class TarDataStreamTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        // enshure triggering autoload,
        // @see TarDataStream.php line 11
        new TarDataStream(); 
    }

    public function testMainUsage()
    {
        $parent_stream = tmpfile();
        fwrite($parent_stream, "ABCDEFGHIJKLMNOPQRSTUV");
        fseek($parent_stream, 4);
        $context = stream_context_create(['tardatastream'=>['stream'=>$parent_stream]]);

        $substream = fopen("tardatastream://10", "rw", false, $context);
        $data = fread($substream, 1024);

        $this->assertEquals("EFGHIJKLMN", $data);
    }

    public function testGetContents()
    {
        $parent_stream = tmpfile();
        fwrite($parent_stream, "ABCDEFGHIJKLMNOPQRSTUV");
        fseek($parent_stream, 4);
        $context = stream_context_create(['tardatastream'=>['stream'=>$parent_stream]]);

        $substream = fopen("tardatastream://10", "rw", false, $context);
        $data = stream_get_contents($substream);

        $this->assertEquals("EFGHIJKLMN", $data);
    }

    public function testWrite()
    {
        $parent_stream = tmpfile();
        fwrite($parent_stream, "ABCDEFGHIJKLMNOPQRSTUV");
        fseek($parent_stream, 4);
        $context = stream_context_create(['tardatastream'=>['stream'=>$parent_stream]]);

        $substream = fopen("tardatastream://10", "rw", false, $context);
        fwrite($substream, "------");

        fseek($parent_stream, 0);
        $data = fread($parent_stream, 1024);

        $this->assertEquals("ABCD------KLMNOPQRSTUV", $data);
    }

    public function testZeroStream()
    {
        $parent_stream = tmpfile();
        fwrite($parent_stream, "ABCDEFGHIJKLMNOPQRSTUV");
        fseek($parent_stream, 4);
        $context = stream_context_create(['tardatastream'=>['stream'=>$parent_stream]]);

        $substream = fopen("tardatastream://0", "rw", false, $context);
        $subdata = fread($substream, 6);

        fseek($parent_stream, 0);
        $data = fread($parent_stream, 1024);

        $this->assertEquals("", $subdata);
        $this->assertEquals("ABCDEFGHIJKLMNOPQRSTUV", $data);
    }

    /**
     * @group hook
     */
    public function testSeekHook()
    {
        $parent_stream = tmpfile();
        fwrite($parent_stream, "ABCDEFGHIJKLMNOPQRSTUV");
        fseek($parent_stream, 4);

        $tail = 0;
        $hook = function ($tail_size) use (&$tail) {
            $tail = $tail_size;
        };

        $context = stream_context_create(
            ['tardatastream'=>[
                'stream'=>$parent_stream,
                'on_seek_hook'=>$hook,
                ]
            ]
        );

        $substream = fopen("tardatastream://15", "rw", false, $context);
        $tail0 = 0 + $tail;

        fread($substream, 512); // > 15
        $tail1 = 0 + $tail;


        $this->assertEquals(15, $tail0);
        $this->assertEquals(0, $tail1);
    }

    public function testOpenNoContextFails()
    {
        //$this->expectError(E_USER_NOTICE);

        $substream = @fopen("tardatastream://10", "rw");

        $this->assertFalse($substream);
    }

    public function testOpenBadContextFails()
    {
        $context = stream_context_create(['tardatastream'=>['stream'=>array() ]]);
        // $this->expectError(E_USER_NOTICE);

        $substream = @fopen("tardatastream://10", "rw", false, $context);

        $this->assertFalse($substream);
    }

    public function testBadSizeFails()
    {
        $parent_stream = tmpfile();
        $context = stream_context_create(['tardatastream'=>['stream'=>$parent_stream]]);

        $substream1 = @fopen("tardatastream://-8", "rw", false, $context);
        $substream2 = @fopen("tardatastream://asddjoi3js", "rw", false, $context);

        $this->assertFalse($substream1);
        $this->assertFalse($substream2);
    }
}
