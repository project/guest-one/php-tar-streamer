<?php

require_once __DIR__ . "/../src/TarStreamer.php";
require_once __DIR__ . "/../src/TarDataStream.php";

$in = gzopen("php://stdin", "rb");
$out = fopen("php://stderr", "wt");

$tar = new \GO\Tar\TarStreamer($in);

while (false !== $header = $tar->readHeader()) {
    fwrite($out, $header['pathname'] . ":" . PHP_EOL);

    $handle = $tar->getDataStream();
    while (!feof($handle)) {
        $data = fread($handle, 80);
        fwrite($out, "  " . base64_encode($data) . PHP_EOL);
    }
}
