<?php
require_once __DIR__ . "/../src/TarStreamer.php";

$out = gzopen("output.tar.gz", "wb9");
$tar = new \GO\Tar\TarStreamer($out);

echo "Adding directory: ", __DIR__, PHP_EOL;
$tar->addExistingFile("some_dir", __DIR__);

echo "Adding file: ", __FILE__, PHP_EOL;
$tar->addExistingFile("some_dir/file.php", __FILE__);

echo "Closing archive", PHP_EOL;
$tar->addEndOfArchive();
fclose($out);
