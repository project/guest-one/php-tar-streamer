<?php

require __DIR__ . '/_cz.inc.php';

require_once __DIR__ . '/vendor/autoload.php';

echo PHP_EOL, "Splitbrain PHPArchive\Tar test:", PHP_EOL;
echo "(splitbrain/php-archive)", PHP_EOL;
echo "PHP=", PHP_VERSION, PHP_EOL;

$t = microtime(true);

$tar = new \splitbrain\PHPArchive\Tar();
$tar->open($argv[1]);

foreach($tar->extract(sys_get_temp_dir()) as $f) {
    $data = fopen(sys_get_temp_dir(). "/". $f->getPath(), "rb");
    cz(str_pad(basename($f->getPath()), 15, " ", STR_PAD_LEFT), $data);
    fclose($data);
    unlink(sys_get_temp_dir() ."/".$f->getPath());}

echo "time:\t",   microtime(true)-$t, PHP_EOL;
echo "memory:\t", memory_get_peak_usage(true),PHP_EOL;