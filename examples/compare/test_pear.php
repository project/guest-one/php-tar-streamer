<?php

require __DIR__ . '/_cz.inc.php';

require_once __DIR__ . '/vendor/autoload.php';

echo PHP_EOL, "PEAR Tar test:", PHP_EOL;
echo "(pear/archive_tar)", PHP_EOL;
echo "PHP=", PHP_VERSION, PHP_EOL;

$t = microtime(true);

$tar = new Archive_Tar($argv[1]);
$list = [];
foreach ($tar->listContent() as $f) {
    $list[] = $f["filename"];
}
$tar->extractList($list, sys_get_temp_dir());

foreach ($list as $name) {
    $data = fopen(sys_get_temp_dir(). "/". $name, "rb");
    cz(str_pad(basename($name), 15, " ", STR_PAD_LEFT), $data);
    fclose($data);
    unlink(sys_get_temp_dir() ."/". $name);
}

echo "time:\t",   microtime(true)-$t, PHP_EOL;
echo "memory:\t", memory_get_peak_usage(true),PHP_EOL;
