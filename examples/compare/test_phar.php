<?php

require __DIR__ . '/_cz.inc.php';

echo PHP_EOL, "PharData test:", PHP_EOL;
echo "(builtin class)", PHP_EOL;
echo "PHP=", PHP_VERSION, PHP_EOL;

$t = microtime(true);

$tar = new PharData($argv[1]);

foreach ($tar as $f) {
    $data = fopen($f, "rb");
    cz(str_pad(basename($f), 15, " ", STR_PAD_LEFT), $data);
}

echo "time:\t",   microtime(true)-$t, PHP_EOL;
echo "memory:\t", memory_get_peak_usage(true),PHP_EOL;
