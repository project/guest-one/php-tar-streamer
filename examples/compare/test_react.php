<?php
require __DIR__ . '/_cz.inc.php';

require_once __DIR__ . '/vendor/autoload.php';

use React\EventLoop\Factory;
use React\Stream\ReadableResourceStream;

echo PHP_EOL, "Clue's React\Tar\Decoder test:", PHP_EOL;
echo "(clue/tar-react)", PHP_EOL;
echo "PHP=", PHP_VERSION, PHP_EOL;

$decoder = new\ Clue\React\Tar\Decoder();

$decoder->on('entry', function (array $header, \React\Stream\ReadableStreamInterface $file) {
    echo 'File ' . $header['filename'];
    echo ' (' . $header['size'] . ' bytes):' . PHP_EOL;

    // $file->on('data', function ($chunk) {
    //     echo "data";
    // });
});

$in = gzopen($argv[1], 'r');

$loop = Factory::create();
$stream = new ReadableResourceStream( $in, $loop);
$stream->pipe($decoder);
$loop->run();

echo "time:\t",   microtime(true)-$t, PHP_EOL;
echo "memory:\t", memory_get_peak_usage(true),PHP_EOL;

