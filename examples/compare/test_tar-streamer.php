<?php

require __DIR__ . '/_cz.inc.php';

echo PHP_EOL, "TarStreamer test:", PHP_EOL;
echo "(this package)", PHP_EOL;
echo "PHP=", PHP_VERSION, PHP_EOL;

require_once __DIR__ . "/../../src/TarStreamer.php";
require_once __DIR__ . "/../../src/TarDataStream.php";

$t = microtime(true);

$out = gzopen($argv[1], "rt");
$tar = new \GO\Tar\TarStreamer($out);

while (true) {
    $header = $tar->readHeader();
    if($tar->eof()) {break;}
    $data   = $tar->getDataStream();
    cz(str_pad(basename($header["pathname"]), 15, " ", STR_PAD_LEFT), $data);
}

echo "time:\t",   microtime(true)-$t, PHP_EOL;
echo "memory:\t", memory_get_peak_usage(true),PHP_EOL;

