#!/bin/bash

cd $(dirname "$0")

if [[ ! -d vendor ]]
then
    composer install --no-dev
fi

if [[ ! -f test.tar.gz ]]
then
    truncate -s 10M  z10mb.bin
    truncate -s 100M z100mb.bin
    dd if=/dev/urandom of=r10mb.bin bs=1024 count=10240
    dd if=/dev/urandom of=r100mb.bin bs=1024 count=102400
    tar -czf test.tar.gz z10mb.bin z100mb.bin r10mb.bin r100mb.bin
    rm z10mb.bin z100mb.bin r10mb.bin r100mb.bin
fi

PHP_BIN=php

php test_pear.php test.tar.gz
php test_phar.php test.tar.gz
php test_tar-streamer.php test.tar.gz
php test_splitbrain.php test.tar.gz
# php test_react.php # no chances
