<?php




function find_word($filename_tar_gz, $word)
{

    $tar = new \RecursiveIteratorIterator( new \PharData($filename_tar_gz) );
    $result = 0;
    $cnt = 0;
    echo "  ";
    foreach ($tar as $f) {
        if(!$f->isFile()) {
            continue;
        }
        $fd  = fopen($f, "rb");
        $tail = "";
        while (!feof($fd)) {
            printf("\e[2D%s ", (['-', '\\', '|', '/'])[++$cnt%4]);
            $data = fread($fd, 1024);
            list($tail, $inc) = count_words($tail, $data, $word);
            $result += $inc;
        }
        fclose($fd);
    }
    echo "\e[2D  \e[2D";
    return $result;
}

echo "Testing builtin PharData", PHP_EOL;
require __DIR__ . '/_common.php';
