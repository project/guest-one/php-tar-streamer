<?php

require_once __DIR__ . "/../../src/TarStreamer.php";
require_once __DIR__ . "/../../src/TarDataStream.php";

function find_word($filename_tar_gz, $word)
{
    switch (pathinfo($filename_tar_gz, PATHINFO_EXTENSION)){
        case 'bz2':
            $in  = bzopen($filename_tar_gz, "rb"); // require php-ext-bz2
            break;
        case 'xz':
            $in = xzopen($filename_tar_gz, "rb"); // require php-ext-xz
            break;
        default:
            $in  = gzopen($filename_tar_gz, "rb");
            break;
    }
    $tar = new \GO\Tar\TarStreamer($in);
    $result = 0;
    $cnt = 0;
    echo "  ";
    while (false !== ($header = $tar->readHeader())) {
        if ($header['_type'] !== '0') {
            continue;
        }
        $fd  = $tar->getDataStream();
        $tail = "";
        while (!feof($fd)) {
            printf("\e[2D%s ", (['-', '\\', '|', '/'])[++$cnt%4]);
            $data = fread($fd, 1024);
            list($tail, $inc) = count_words($tail, $data, $word);
            $result += $inc;
        }
        fclose($fd);
    }
    echo "\e[2D  \e[2D";
    return $result;
}
echo "Testing TarStreamer", PHP_EOL;
require __DIR__ . '/_common.php';
