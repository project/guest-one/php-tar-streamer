<?php

declare(strict_types=1);

$URLS = [
    'http://museum.php.net/php5/php-5.0.0.tar.gz', // ~ 5M
    'https://www.php.net/distributions/php-7.2.0.tar.gz', // ~ 18M
    'http://mirror.linux-ia64.org/gnu/gcc/releases/gcc-9.2.0/gcc-9.2.0.tar.gz', // ~ 119M
    'https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.9.325.tar.gz', // ~ 136M
];
$WORD = 'trash';

//Word 'trash' has been found 210 times

function download($url, $filename)
{
    if (!file_exists($filename)) {
        echo "Downloading $filename ...", PHP_EOL;
        $out = fopen($filename, "wt");
        $c = curl_init($URL);
        curl_setopt($c, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($c, CURLOPT_FILE, $out);
        curl_exec($c) || die();
        curl_close($c);
        fclose($out);
    }
}

function fail_at_any($errno,$message,$file,$line,$context)
{
    debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
    throw new Exception($message);
    return false;
}

function count_words($tail, $buffer, $word)
{
    $result = 0;
    $from = 0;
    $data = $tail . $buffer;
    while (false !== ($found = strpos($data, $word, $from))) {
        $from = $found + strlen($word);
        ++$result;
    }
    if ($from > 0) {
        return [substr($data, $from), $result];
    } else {
        return [$buffer, $result];
    }
}

set_error_handler("fail_at_any");

foreach ($URLS as $URL) {
    $FILENAME = basename($URL);
    download($URL, $FILENAME);
    echo "PHP=", PHP_VERSION, PHP_EOL;
    echo "Processing $FILENAME ...", PHP_EOL;

    $t = microtime(true);

    try {
        $words = find_word($FILENAME, $WORD);
    } catch (Exception $e) {
        echo "EXCEPTION: ", get_class($e), PHP_EOL;
        echo $e->getMessage(), PHP_EOL;
        echo PHP_EOL;
        continue;
    }

    echo "Word '$WORD' has been found ", $words, " times", PHP_EOL;

    echo "time:\t", sprintf("%0.4f sec", microtime(true) - $t), PHP_EOL;
    echo "memory:\t", memory_get_peak_usage(true), " bytes", PHP_EOL;
    echo PHP_EOL;
}
