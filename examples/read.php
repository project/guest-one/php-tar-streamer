<?php
require_once __DIR__ . "/../src/TarStreamer.php";

$in = gzopen("output.tar.gz", "rb");
$tar = new \GO\Tar\TarStreamer($in);

echo "Archive contents:", PHP_EOL;
while (false !== $header = $tar->readHeader()) {
    echo " ",$header['pathname'], PHP_EOL;
}

