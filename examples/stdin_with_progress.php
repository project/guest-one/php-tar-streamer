<?php

require_once __DIR__ . "/../src/TarStreamer.php";
require_once __DIR__ . "/../src/TarDataStream.php";

$in = gzopen("php://stdin", "rb");
$tar = new \GO\Tar\TarStreamer($in);

function progressCallback($seek_position, $file_size)
{
    echo "[{$seek_position}/{$file_size}]", PHP_EOL;
}

echo "Archive contents:", PHP_EOL;
while (false !== $header = $tar->readHeader()) {
    echo " ", $header['pathname'], PHP_EOL;

    $handle = $tar->getDataStream("progressCallback");
    // NOTE! small chuncks (r/w sections) slow down I/O operations
    stream_set_chunk_size($handle, 30);
    $data = [];
    while (!feof($handle)) {
        $data[] = base64_encode(fread($handle, 30));
    }
    echo PHP_EOL . implode(PHP_EOL, $data) . PHP_EOL;
}


