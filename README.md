# TarStreamer — stream-based tar processor.

This library was created to solve folowing problems:

  - Working with very large (gigabytes) tar archives on limited RAM.
  - Processing archives coming via unix pipes.

## Usage

Installing:

```bash
composer require guest-one/php-tar-streamer
```

Creating new .tar.gz:

```php
$out = gzopen("output.tar.gz", "wb9");
$tar = new \GO\Tar\TarStreamer($out);

// Adding existing empty folder as "some_dir"
$tar->addExistingFile("some_dir", "/tmp");

// Adding existing file as "some_dir/file.php"
$tar->addExistingFile("some_dir/file.php", __FILE__);

// Adding end-of-archive marker
$tar->addEndOfArchive();
fclose($out);
```

Extracting .tar.gz coming from STDIN and displaying to STDERR:

```php
$in  = gzopen("php://stdin", "rb");
$out = fopen("php://stderr", "wt");

$tar = new \GO\Tar\TarStreamer($in);

while (false !== $header = $tar->readHeader()) {
    fwrite($out, $header['pathname']. ":". PHP_EOL);
    $handle = $tar->getDataStream();
    while (!feof($handle)) {
        $data = fread($handle, 80);
        fwrite($out, "  ". base64_encode($data). PHP_EOL);
    }
}
```

## Compare to other packages

See details in [examples](examples/compare).

**TL/DR:** PharData is fast, but uses RAM. TarStreamer is fast and preserves RAM.

```
$ ./run.sh

PEAR Tar test:
(pear/archive_tar)
PHP=7.4.28
      z10mb.bin in 81921 reads
     z100mb.bin in 819201 reads
      r10mb.bin in 81921 reads
     r100mb.bin in 819201 reads
time:   2.1175360679626
memory: 2097152

PharData test:
(builtin class)
PHP=7.4.28
     r100mb.bin in 819200 reads
      r10mb.bin in 81920 reads
     z100mb.bin in 819200 reads
      z10mb.bin in 81920 reads
time:   1.0884189605713
memory: 10485760

TarStreamer test:
(this package)
PHP=7.4.28
      z10mb.bin in 81920 reads
     z100mb.bin in 819200 reads
      r10mb.bin in 81920 reads
     r100mb.bin in 819200 reads
time:   0.97866702079773
memory: 2097152

Splitbrain PHPArchive\Tar test:
(splitbrain/php-archive)
PHP=7.4.28
      z10mb.bin in 81921 reads
     z100mb.bin in 819201 reads
      r10mb.bin in 81921 reads
     r100mb.bin in 819201 reads
time:   1.76238489151
memory: 2097152
```

## Limitations

 - The only supprted version of tar is "ustar"
 - Only "directory", "file" and "symlink" entry types are fully supported
 - Package was not properly tested on non-Linux environments
