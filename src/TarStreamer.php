<?php

/**
 * TarStreamer - memory-efficient tar archive class
 *
 * (c) 2020 EugenePopov <popov_e_n@mail.ru>
 */
declare(strict_types=1);

namespace GO\Tar;

class TarStreamer
{

    const HEADER_FORMAT_UNPACK = "a100name/" . "a8mode/" . "a8uid/" . "a8gid/" . "a12size/" . "a12mtime/" . "a8checksum/" .
            "a1type/" . "a100link/" . "a6ustar/" . "a2version/" . "a32uname/" . "a32gname/" .
            "a8devmajor/" . "a8devminor/" . "a155prefix";
    const HEADER_FORMAT_PACK = "a100a8a8a8a12a12a" .
            "8a1a100a" .
            "6a2a32a32" .
            "a8a8a155";

    protected $stream;
    protected $options;
    protected $headerRead = false;
    protected $lastHeader = false;
    protected $unreadTailSize = null;
    protected $eof = false;
    protected $headerIndex = 0;

    public function __construct($stream, $options = 0)
    {
        $this->stream = $stream;
        $this->options = 0;
    }

    public function addExistingFile($nameInArchive, $pathToFile)
    {
        $stat = @\lstat($pathToFile);
        if ($stat === false) {
            throw new \Exception("Failed to stat file: $pathToFile");
        }
        switch ($this->howToAdd($stat)) {
            case 'Stream':
                $stream = \fopen($pathToFile, "rb");
                if ($stream === false) {
                    throw new \Exception("Failed to read file: $pathToFile");
                }
                $this->addStream($stat, $nameInArchive, $stream);
                \fclose($stream);
                break;
            case 'Symlink':
                $linkTo = \readlink($pathToFile);
                if ($linkTo === false) {
                    throw new \Exception("Failed to read symlink: $pathToFile");
                }
                $this->addSymlink($stat, $nameInArchive, $linkTo);
                break;
            case 'NonFile':
                $this->addNonFile($stat, $nameInArchive);
                break;
        }
    }

    public function addStream($fileStat, $nameInArchive, $dataStream)
    {
        \fwrite($this->stream, $this->createHeader($nameInArchive, $fileStat));
        $size = \stream_copy_to_stream($dataStream, $this->stream);
        if ($size === false) {
            throw new \Exception("Error during writing");
        }
        \fwrite($this->stream, $this->createPadding($size));
        $this->headerIndex++;
    }

    public function addSymlink($fileStat, $nameInArchive, $linkTo)
    {
        $fileStat['_link'] = $linkTo;
        \fwrite($this->stream, $this->createHeader($nameInArchive, $fileStat));
        $this->headerIndex++;
    }

    public function addNonFile($fileStat, $nameInArchive)
    {
        \fwrite($this->stream, $this->createHeader($nameInArchive, $fileStat));
        $this->headerIndex++;
    }

    public function addEndOfArchive()
    {
        \fwrite($this->stream, pack('a1024', ''));
    }

    public function eof()
    {
        return $this->eof;
    }

    protected function howToAdd($fileStat)
    {
        $type = 0770000 & $fileStat['mode'];
        if ($type === 0100000) {
            return 'Stream';
        }
        if ($type === 0120000) {
            return 'Symlink';
        }
        return 'NonFile';
    }

    protected function typeStatToTar($statType)
    {
        $type = 0770000 & $statType;

        switch ($type) {
            case 0100000: // file
                return '0';
            case 0120000: // symlink
                return '2';
            case 0040000: // dir
                return '5';
            case 0010000: // fifo
                return '6';
            default:
                return '0';
        }
    }

//    public function typeTarToStat($statType) { }

    protected function trimz($str)
    {
        $p = strpos($str, "\0");
        if ($p === false) {
            return $str;
        } else {
            return \substr($str, 0, $p);
        }
    }

    protected function assertRawStatValid($rawStat)
    {
        $oct = ['mode', 'uid', 'gid', 'size', 'mtime',
            'checksum', 'devmajor', 'devminor'];

        foreach ($oct as $key) {
            if (!preg_match('#^[0-7 \0]+$#S', $rawStat[$key])) {
                var_dump(preg_split('##S', $rawStat[$key]));
                throw new \Exception("Header field '{$key}' does not looks like octal number: '{$rawStat[$key]}'");
            }
        }
    }

    protected function normalizeRawStatValid($rawStat)
    {
        $oct = ['mode', 'uid', 'gid', 'size', 'mtime',
            'checksum', 'devmajor', 'devminor'];

        foreach ($oct as $key) {
            $rawStat[$key] = preg_replace('#[ \0]#S', '', $rawStat[$key]);
        }
        return $rawStat;
    }

    /**
     *  Reads and returns next file header from archive.
     *  Throws exception on error.
     *  Returns FALSE if reading was past EOF.
     * @return array | false
     * @throws \Exception
     */
    public function readHeader()
    {
        if ($this->eof) {
            return false;
        }
        if ($this->headerRead) {
            $this->skipDataSection();
        } else {
            $this->skipDataTailSection();
        }
        $this->unreadTailSize = null;

        $header = \fread($this->stream, 512);
        $this->headerRead = true;

        if (\strlen($header) !== 512) {
            throw new \Exception("Header section too short: " . \strlen($header));
        }
        $chkHeader = \substr($header, 0, 148) . '        ' . \substr($header, 156);
        $expSum = 0;
        for ($i = 0;
                $i < 512;
                $i++) {
            $expSum += \ord(substr($chkHeader, $i, 1));
        }

        $rawStat = \unpack(self::HEADER_FORMAT_UNPACK, $header);
        $this->assertRawStatValid($rawStat);
        $rawStat = $this->normalizeRawStatValid($rawStat);
        $fileStat = [
            0 => -1,
            1 => 0,
            2 => \octdec($rawStat['mode']),
            3 => -1,
            4 => \octdec($rawStat['uid']),
            5 => \octdec($rawStat['gid']),
            6 => -1,
            7 => \octdec($rawStat['size']),
            8 => \octdec($rawStat['mtime']),
            9 => \octdec($rawStat['mtime']),
            10 => \octdec($rawStat['mtime']),
            11 => -1,
            12 => -1,
            'dev' => -1,
            'ino' => 0,
            'mode' => \octdec($rawStat['mode']),
            'nlink' => -1,
            'uid' => \octdec($rawStat['uid']),
            'gid' => \octdec($rawStat['gid']),
            'rdev' => -1,
            'size' => \octdec($rawStat['size']),
            'atime' => \octdec($rawStat['mtime']),
            'mtime' => \octdec($rawStat['mtime']),
            'ctime' => \octdec($rawStat['mtime']),
            'blksize' => -1,
            'blocks' => -1,
            '_checksum' => \octdec($rawStat['checksum']),
            '_type' => $rawStat['type'],
            '_link' => $this->trimz($rawStat['link']),
            '_ustar' => \substr($rawStat['ustar'], 0, 5),
            '_version' => $rawStat['version'],
            '_uname' => $this->trimz($rawStat['uname']),
            '_gname' => $this->trimz($rawStat['gname']),
            '_devmajor' => \octdec($rawStat['devmajor']),
            '_devminor' => \octdec($rawStat['devminor']),
            '_name' => $this->trimz($rawStat['name']),
            '_prefix' => $this->trimz($rawStat['prefix']),
            '_final' => false,
            'pathname' => null,
        ];
        if (strlen($fileStat['_prefix'])) {
            if (strlen($fileStat['_name'])) {
                $fileStat['pathname'] = $fileStat['_prefix'] . '/' . $fileStat['_name'];
            } else {
                $fileStat['pathname'] = $fileStat['_prefix'];
            }
        } else {
            $fileStat['pathname'] = $fileStat['_name'];
        }
        $this->lastHeader = $fileStat;

        if (($fileStat['_checksum'] === 0) && ($expSum === 32 * 8)) {
            $fileStat['_final'] = true;
            $this->eof = true;
            return false;  // EOF
        }
        if ($fileStat['_ustar'] !== "ustar") {
            throw new \Exception("Tar is not ustar (bad magic): [" . $fileStat['_ustar'] . "]");
        }
        if ($fileStat['_checksum'] !== $expSum) {
            throw new \Exception("Bad header checksum: $expSum != " . $fileStat['_checksum']);
        }
        $this->headerIndex++;
        return $fileStat;
    }

    public function readDataToStream($outputStream)
    {
        if (!$this->headerRead) {
            $this->readHeader();
        }
        $size = $this->lastHeader['size'];
        $this->headerRead = false;
        $this->lastHeader = null;
        $this->unreadTailSize = null;

        if ($size == 0) {
            return;
        }
        $copied = \stream_copy_to_stream($this->stream, $outputStream, $size);

        if ($copied === false || $copied !== $size) {
            throw new \Exception("Copy failed: " . $copied);
        }

// seek padding
        if ($size % 512 !== 0) {
            \fread($this->stream, (512 - $size % 512)); // seek forward
        }
    }

    public function readData($maxSize = 4096)
    {
        if (!$this->headerRead) {
            $this->readHeader();
        }
        $size = $this->lastHeader['size'];
        $this->headerRead = false;
        $this->lastHeader = null;
        $this->unreadTailSize = null;

        if ($maxSize > 0 && $size > $maxSize) {
            throw new \Exception("Use copyDataToStream() for big files!");
        }

        if ($size === 0) {
            return null;
        }
        $data = \fread($this->stream, $size);
// seek padding
        if ($size % 512 !== 0) {
            \fread($this->stream, (512 - $size % 512)); // seek forward
        }
        return $data;
    }

    /**
     *  Returns readable stream (resource) corresponding to the current
     * file inside archive.
     *  Due to streamable nature of TAR the resource is valid ONLY until
     * the next call of readXXX or getDataStream methods.
     * @param $onStreamReadCallback null | callable
     * @return resource | false
     */
    public function getDataStream($onStreamReadCallback = null)
    {
        if ($this->eof) {
            return false;
        }
        if (!$this->headerRead) {
            $header = $this->readHeader();
        }
        $size = $this->lastHeader['size'];
        if ($size % 512 !== 0) {
            $padding = (512 - $size % 512);
        } else {
            $padding = 0;
        }
        $this->headerRead = false;
        $this->lastHeader = null;

        $obj = $this;
        $context = stream_context_create(
                ['tardatastream' => [
                        'stream' => $this->stream,
                        'on_seek_hook' => function ($tail) use ($obj, $padding, $size, $onStreamReadCallback) {
                            $obj->unreadTailSize = $tail + $padding;
                            if ($onStreamReadCallback !== null) {
                                \call_user_func($onStreamReadCallback, $size - $tail, $size);
                            }
                        },
                    ]
                ]
        );

        $dataStream = fopen(TarDataStream::WRAPPER_PREFIX . "://" . $size, "rb", false, $context);
        return $dataStream;
    }

    protected function skipDataSection()
    {
        $size = $this->lastHeader['size'];
        $this->headerRead = false;
        $this->lastHeader = null;

        if ($size === 0) {
            return;
        }
        \fread($this->stream, $size); // seek forward
        // seek padding
        if ($size % 512 !== 0) {
            \fread($this->stream, (512 - $size % 512)); // seek forward
        }
    }

    protected function skipDataTailSection()
    {
        if ($this->unreadTailSize === null) {
            return;
        }
        $size = $this->unreadTailSize;
        $this->headerRead = false;
        $this->lastHeader = null;

        if ($size > 0) {
            \fread($this->stream, $size); // seek forward
        }
    }

    protected function formatLongPath($path)
    {
        $seg = \explode("/", $path);
        $prefix = [];
        $suffix = [];
        $len = 0;
        foreach ($seg as $dir) {
            if ($len < 155) {
                if ($len === 0 && \strlen($dir) > 155) {
                    throw new \Exception("Prefix too long: " . $path);
                }
                $len += \strlen($dir);
                $prefix[] = $dir;
            } else {
                $suffix[] = $dir;
            }
        }
        return [\implode("/", $prefix), \implode("/", $suffix)];
    }

    protected function createHeader($fileName, $fileStat)
    {
// order matters!
        $fields = [
            "name" => "",
            "mode" => \sprintf('%6s ', \decoct($fileStat['mode'])),
            "uid" => \sprintf('%6s ', \decoct($fileStat['uid'])),
            "gid" => \sprintf('%6s ', \decoct($fileStat['gid'])),
            "size" => \sprintf('%11s ', \decoct($fileStat['size'])),
            "mtime" => \sprintf('%11s ', \decoct($fileStat['mtime'])),
            "checksum" => '        ',
            "type" => "",
            "link" => "",
            "ustar" => "ustar",
            "version" => "",
            "uname" => "",
            "gname" => "",
            "devmajor" => "",
            "devminor" => "",
            "prefix" => "",
        ];

        if (\strlen($fileName) <= 100) {
            $fields["name"] = $fileName;
        } elseif (\strlen($fileName) <= 256) {
            list($fields["prefix"], $fields["name"]) = $this->formatLongPath($fileName);
        } else {
            throw new \Exception("Filename too long: (" . \strlen($fileName) . ") " . $fileName);
        }

        $fields["type"] = $this->typeStatToTar($fileStat['mode']);
        if ($fields["type"] !== '0') {
            $fields["size"] = \str_repeat('0', 11);
        }

        if (isset($fileStat['_link'])) {
            $fields["type"] = '2';
            $fields["link"] = $fileStat['_link'];
        }

        $user = \posix_getpwuid($fileStat['uid']);
        $group = \posix_getgrgid($fileStat['gid']);

        if ($user === false) {
            $fields["uname"] = "nobody";
        } else {
            $fields["uname"] = \sprintf('%32s ', $user["name"]);
        }
        if ($group === false) {
            $fields["gname"] = "nobody";
        } else {
            $fields["gname"] = \sprintf('%32s ', $group["name"]);
        }

        $header = \str_pad(\pack(self::HEADER_FORMAT_PACK, ...\array_values($fields)), 512, "\0", STR_PAD_RIGHT);
        $sum = 0;

        for ($i = 0;
                $i < 512;
                $i++) {
            $sum += \ord(\substr($header, $i, 1));
        }

        $sum_ord = \sprintf('%08s', \decoct($sum));
        return \substr_replace($header, $sum_ord, 148, 8);
    }

    protected function createPadding($size)
    {
        return $size > 0 ? \pack('a' . (512 - $size % 512), '') : '';
    }
}
