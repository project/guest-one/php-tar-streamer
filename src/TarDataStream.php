<?php

/**
 * TarDataStreamer - memory-efficient tar archive class
 *
 * (c) 2020 EugenePopov <popov_e_n@mail.ru>
 */
declare(strict_types=1);

namespace GO\Tar;

stream_wrapper_register(TarDataStream::WRAPPER_PREFIX, TarDataStream::class);

class TarDataStream /* implemets streamWrapper */
{
    /**
     * @var resource
     */
    public $context;

    public const WRAPPER_PREFIX = "tardatastream";

    protected $stream = null;
    protected $position = 0;
    protected $size = 0;
    protected $seekable = false;
    protected $on_seek_hook = null;

    public function stream_eof(): bool
    {
        return $this->position >= $this->size;
    }

    public function stream_open($path, $mode, $options, &$opened_path): bool
    {
        // url = "substream://2371623" => size=2371623
        list($prefix, $size) = explode("://", $path, 2);
        $opts = stream_context_get_options($this->context);
        if (!isset($opts[$prefix]['stream'])) {
            trigger_error("No parent stream set in context.", E_USER_NOTICE);
            return false;
        }

        if (!is_resource($opts[$prefix]['stream'])) {
            trigger_error("Parent stream must be resource.", E_USER_NOTICE);
            return false;
        }

        if (!preg_match('#^\d+$#s', $size)) {
            trigger_error("Bad size in path: [$path]. Must be integer.", E_USER_NOTICE);
            return false;
        }

        if (isset($opts[$prefix]['on_seek_hook'])) {
            $this->on_seek_hook = $opts[$prefix]['on_seek_hook'];
        }

        $this->stream = $opts[$prefix]['stream'];
        $this->size = (int) $size;

        $this->on_position_changed();

        return true;
    }

    public function stream_read(int $count): string
    {
        if ($this->position + $count > $this->size) {
            $count = $this->size - $this->position;
        }

        if ($count <= 0) {
            return "";
        }
        $data = fread($this->stream, $count);
        $this->position += strlen($data);
        $this->on_position_changed();
        return $data;
    }

    public function stream_write(string $data): int
    {
        $res = fwrite($this->stream, $data);
        $this->position += $res;
        $this->on_position_changed();
        return $res;
    }

    /**
     * @see https://www.php.net/manual/en/function.stat.php
     * @return array|false
     */
    public function stream_stat()
    {
        $r = [
            0 => 0, //device number ***
            1 => 0, //inode number ****
            2 => 0666, //inode protection mode *****
            3 => 1, //number of links
            4 => 0, //userid of owner *
            5 => 0, //groupid of owner *
            6 => 0, //device type, if inode device
            7 => $this->size, //size in bytes
            8 => 0, //time of last access (Unix timestamp)
            9 => 0, //time of last modification (Unix timestamp)
            10 => 0, //time of last inode change (Unix timestamp)
            11 => 1024, //blocksize of filesystem IO **
            12 => (int) ceil((float) $this->size / 1024.0), //number of 512-byte blocks allocated **
        ];
        $r['dev'] = $r[0];
        $r['ino'] = $r[1];
        $r['mode'] = $r[2];
        $r['nlink'] = $r[3];
        $r['uid'] = $r[4];
        $r['gid'] = $r[5];
        $r['rdev'] = $r[6];
        $r['size'] = $r[7];
        $r['atime'] = $r[8];
        $r['mtime'] = $r[9];
        $r['ctime'] = $r[10];
        $r['blksize'] = $r[11];
        $r['blocks'] = $r[12];

        return $r;
    }

    protected function get_tail_size()
    {
        return $this->size - $this->position;
    }

    protected function on_position_changed()
    {
        if ($this->on_seek_hook !== null) {
            \call_user_func($this->on_seek_hook, $this->get_tail_size());
        }
    }
}
