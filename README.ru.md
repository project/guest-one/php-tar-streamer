# TarStreamer - потоковая работа с tar-архивамми

Эта библиотека создана чтобы решить две проблемы:

  - Работа с очень большими (десятки гигабайт) tar-архивами на ограниченной памяти.
  - Работа с tar-архивами. передаваемыми через unix-пайпы.

## Использование

Установка:

```bash
composer require guest-one/php-tar-streamer
```

Создание архива .tar.gz:

```php
$out = gzopen("output.tar.gz", "wb9");
$tar = new \GO\Tar\TarStreamer($out);

// Добавлем пустой каталог как "some_dir"
$tar->addExistingFile("some_dir", "/tmp");

// Добавлем существующий файл "some_dir/file.php"
$tar->addExistingFile("some_dir/file.php", __FILE__);

// Добавляем марке конца архива.
$tar->addEndOfArchive();
fclose($out);
```

Распаковка архива .tar.gz из STDIN и вывод в STDERR:

```php
$in  = gzopen("php://stdin", "rb");
$out = fopen("php://stderr", "wt");

$tar = new \GO\Tar\TarStreamer($in);

while (true) {
    $header = $tar->readHeader();
    if ($tar->eof()) {
        break;
    }
    fwrite($out, $header['pathname']. ":". PHP_EOL);
    $handle = $tar->getDataStream();
    while (!feof($handle)) {
        $data = fread($handle, 80);
        fwrite($out, "  ". base64_encode($data). PHP_EOL);
    }
}
```

## Сравнение с другими пакетами

Подробности в [примерах](examples/compare)

**TL/DR:** PharData очень быстрый, но потребляет память. TarStreamer тоже быстрый и память не расходует.

```
$ ./run.sh

PEAR Tar test:
(pear/archive_tar)
PHP=7.4.28
      z10mb.bin in 81921 reads
     z100mb.bin in 819201 reads
      r10mb.bin in 81921 reads
     r100mb.bin in 819201 reads
time:   2.1175360679626
memory: 2097152

PharData test:
(builtin class)
PHP=7.4.28
     r100mb.bin in 819200 reads
      r10mb.bin in 81920 reads
     z100mb.bin in 819200 reads
      z10mb.bin in 81920 reads
time:   1.0884189605713
memory: 10485760

TarStreamer test:
(this package)
PHP=7.4.28
      z10mb.bin in 81920 reads
     z100mb.bin in 819200 reads
      r10mb.bin in 81920 reads
     r100mb.bin in 819200 reads
time:   0.97866702079773
memory: 2097152

Splitbrain PHPArchive\Tar test:
(splitbrain/php-archive)
PHP=7.4.28
      z10mb.bin in 81921 reads
     z100mb.bin in 819201 reads
      r10mb.bin in 81921 reads
     r100mb.bin in 819201 reads
time:   1.76238489151
memory: 2097152
```

## Limitations

 - Поддерживаются tar-архивы только типа "ustar"
 - Полноценно обрабатываются в архивах только каталоги, файлы и символические ссылки
 - Пакет польноценно тестировался только на Linux

